#define _GNU_SOURCE
#include <assert.h>
#include <stdio.h>
#include <sys/mman.h>

#include "mem.h"
#include "mem_internals.h"

#define HEAP_SIZE 4096
#define HEAP_START ((void*)0x04040000)

extern void debug(const char* fmt, ... );

static struct block_header* block_get_header(void* contents){
	return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

void just_success_malloc(){
    debug("Тест 1:\n");
    debug("Создание кучи размером %d байт\n", HEAP_SIZE);
    void* heap = heap_init(HEAP_SIZE);
    assert(heap);
    debug("Куча создана, размер %d байт\n", HEAP_SIZE);
    debug("1: Выделим несколько байт\n");
    debug("Выделение %d байт\n", 132);
    void* allocated = _malloc(132);
    assert(allocated);
    debug("Выделение успешно!\n");

    heap_term();
    debug("Тест 1 пройден!\n");
}

void malloc_2_blocks_free_1(){
    debug("Тест 2:\n");
    debug("Создание кучи размером %d байт\n", HEAP_SIZE);
    void* heap = heap_init(HEAP_SIZE);
    assert(heap);
    debug("Куча создана, размер %d байт\n", HEAP_SIZE);
    debug("1: Выделим 2 блока\n");
    debug("\tВыделение %d байт\n", 132);
    void* allocated1 = _malloc(132);
    assert(allocated1);
    debug("\tВыделение %d байт\n", 128);
    void* allocated2 = _malloc(128);
    assert(allocated2);
    debug("Выделение успешно!\n");

    debug("2.Удалим первый блок\n");
    _free(allocated1);
    assert(block_get_header(allocated1)->is_free);
    assert(!block_get_header(allocated2)->is_free);

    heap_term();
    debug("Тест 2 пройден!\n");
}

void malloc_3_blocks_free_2(){
    debug("Тест 3:\n");
    debug("Создание кучи размером %d байт\n", HEAP_SIZE);
    void* heap = heap_init(HEAP_SIZE);
    assert(heap);
    debug("Куча создана, размер %d байт\n", HEAP_SIZE);
    debug("1.Выделим 3 блока\n");
    debug("\tВыделение %d байт\n", 132);
    void* allocated1 = _malloc(132);
    assert(allocated1);
    debug("\tВыделение %d байт\n", 128);
    void* allocated2 = _malloc(128);
    assert(allocated2);
    debug("\tВыделение %d байт\n", 190);
    void* allocated3 = _malloc(190);
    assert(allocated3);
    debug("Выделение успешно!\n");

    debug("2.Удалим второй и первый блоки\n");
    _free(allocated2);
    _free(allocated1);
    assert(block_get_header(allocated1)->is_free);
    assert(block_get_header(allocated2)->is_free);
    assert((block_get_header(allocated1)->capacity.bytes == 132 + size_from_capacity((block_capacity){.bytes=128}).bytes));
    assert(!block_get_header(allocated3)->is_free);
    heap_term();
    debug("Тест 3 пройден!\n");
}

void expand_old_region(){
    debug("Тест 4:\n");
    debug("Создание кучи размером %d байт\n", 15);
    void* heap = heap_init(15);
    assert(heap);
    debug("Куча создана, размер %d байт\n", 15);
    debug("1. Выделим память\n");
    debug("Выделение %d байт\n", 132);
    void* allocated1 = _malloc(132);
    assert(allocated1);
    debug("Выделение успешно!\n");

    heap_term();
    debug("Тест 4 пройден!\n");
}

void cant_expand(){
    debug("Тест 5:\n");

    debug("Создание региона\n", 0);
    void* pre_allocated = map_pages(HEAP_START, 8, MAP_FIXED);
    assert(pre_allocated);
    debug("Выделение в занятый регион\n", 0);
    void* alloc_in_filled_ptr = _malloc(123);
    assert(alloc_in_filled_ptr);
    assert(pre_allocated != alloc_in_filled_ptr);

    heap_term();
    debug("Тест 5 пройден!\n");
}

int main() {
    just_success_malloc();
    debug("\n");
    malloc_2_blocks_free_1();
    debug("\n");
    malloc_3_blocks_free_2();
    debug("\n");
    expand_old_region();
    debug("\n");
    cant_expand();
    debug("Все тесты пройдены!\n");
}
