#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query ) {
  size_t alloc_size = region_actual_size(size_from_capacity((block_capacity){.bytes=query}).bytes);
  void* alloc_memory = map_pages(addr, alloc_size, MAP_FIXED );
  if(alloc_memory == MAP_FAILED){
	  alloc_memory = map_pages(addr, alloc_size, 0);
	  if(alloc_memory == MAP_FAILED)
		  return REGION_INVALID;
  }
  block_init(alloc_memory, (block_size){.bytes=alloc_size}, NULL);
  return (struct region) {.addr = alloc_memory, .size = alloc_size, .extends = alloc_memory==addr};
}

static void* block_after( struct block_header const* block )         ;

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd );

struct block_header* delete_region(struct block_header* first_block){
	size_t size = 0;
	struct block_header* cur = first_block;
	while(cur != NULL){
		size += size_from_capacity(cur->capacity).bytes;
		if(!blocks_continuous(cur, cur->next))
			break;
		cur = cur->next;
	}
	if(cur != NULL)
		cur = cur->next;
	else
		cur = NULL;
	munmap(first_block, size);
	return cur;
}
/*  освободить всю память, выделенную под кучу */
void heap_term( ) {
  struct block_header* cur = (struct block_header*) HEAP_START;
  while(cur != NULL) {
	cur = delete_region(cur);
  }
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
  if(block == NULL || !block_splittable(block, query))
	  return false;
  struct block_header* next = block->next;
  block_size first_size = size_from_capacity((block_capacity){.bytes = query});
  block_size second_size = {.bytes = size_from_capacity(block->capacity).bytes - first_size.bytes };
  void* first_block = block;
  void* second_block = (uint8_t*) block + first_size.bytes;
  block_init(first_block, first_size, second_block);
  block_init(second_block, second_size, next);
  return true;
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
  if(block == NULL)
	  return false;
  struct block_header* next_block = block->next;
  if(next_block != NULL && mergeable(block, next_block)) {
	  block->capacity.bytes += size_from_capacity(next_block->capacity).bytes;
	  block_init(block, size_from_capacity(block->capacity), next_block->next);
	  return true;
  }
  return false;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {
  if(block == NULL)
	return (struct block_search_result){.type=BSR_CORRUPTED};
  while (block != NULL) {
    while (try_merge_with_next(block)) {}
    if (block->is_free && block_is_big_enough(sz, block)) {
      return (struct block_search_result) { .type = BSR_FOUND_GOOD_BLOCK, .block = block };
    }
    if(block->next == NULL)
        break;
    block = block->next;
  }
  return (struct block_search_result) { .type = BSR_REACHED_END_NOT_FOUND, .block = block };
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
  struct block_search_result res = find_good_or_last(block, query);
  if (res.type == BSR_FOUND_GOOD_BLOCK) {
    split_if_too_big(res.block, query);
    res.block->is_free = false;
  }
  return res;
}



static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
  if (last == NULL)
	  return NULL;
  void* addr = block_after(last);
  struct region region = alloc_region(addr, query);
  if(region_is_invalid(&region))
	  return NULL;
  last->next = region.addr;
  bool merged = try_merge_with_next(last);
  return merged ? last : last->next;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {

  query = (query > BLOCK_MIN_CAPACITY) ? query : BLOCK_MIN_CAPACITY;
  struct block_search_result res = try_memalloc_existing(query, heap_start);
  if (res.type == BSR_FOUND_GOOD_BLOCK) {
    return res.block;
  }
  else if (res.type == BSR_REACHED_END_NOT_FOUND && res.block) {
    struct block_header* last = res.block;
    struct block_header* grown = grow_heap(last, query);
    struct block_search_result new_result = try_memalloc_existing(query, grown);
    if (new_result.type == BSR_FOUND_GOOD_BLOCK) {
      return new_result.block;
    }
  }
  return NULL;
}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  while(try_merge_with_next(header));
}
